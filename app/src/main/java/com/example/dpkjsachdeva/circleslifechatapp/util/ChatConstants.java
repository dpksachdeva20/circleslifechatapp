package com.example.dpkjsachdeva.circleslifechatapp.util;

public class ChatConstants {

    public static final int VIEW_TYPE_MSG_SENT = 1;
    public static final int VIEW_TYPE_MSG_RECEIVE = 2;


    public static final String JSON_FILE = "data.json";

    public static final String KEY_CHAT = "chat";
    public static final String TIMESTAMP = "timestamp";
    public static final String DIRECTION = "direction";
    public static final String MESSAGE = "message";

    public static final String DIR_OUTGOING = "OUTGOING";
    public static final String DIR_INCOMING = "INCOMING";

    public static final int UPDATE_LIST = 1;
    public static final long ONE_MIN = 60*1000;
    public static final String WAIT_MESSAGE = "Are you there?";

    public static final int START_INDEX = 11;
    public static final int END_INDEX = 16;


}
