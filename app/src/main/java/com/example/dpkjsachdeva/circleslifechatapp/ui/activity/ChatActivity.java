package com.example.dpkjsachdeva.circleslifechatapp.ui.activity;

import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.dpkjsachdeva.circleslifechatapp.R;
import com.example.dpkjsachdeva.circleslifechatapp.ui.fragment.ChatFragment;

public class ChatActivity extends AppCompatActivity {

    ChatFragment mChatFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initView();
    }


    private void initView() {
// get fragment manager
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        mChatFragment = (ChatFragment) fm.findFragmentByTag(ChatFragment.TAG);
        if (mChatFragment == null) {
            mChatFragment = ChatFragment.newInstance();
            ft.add(R.id.fragment_container, mChatFragment, ChatFragment.TAG).commit();
        }


    }

}
