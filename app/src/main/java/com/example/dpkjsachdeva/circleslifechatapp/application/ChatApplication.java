package com.example.dpkjsachdeva.circleslifechatapp.application;

import android.app.Application;

public class ChatApplication extends Application {

    private static ChatApplication sInstance;

    @Override
    public void onCreate() {
        super.onCreate();
        sInstance = this;
    }

    public static ChatApplication getInstance() {
        return sInstance;
    }
}
