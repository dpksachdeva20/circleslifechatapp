package com.example.dpkjsachdeva.circleslifechatapp.model;


import com.example.dpkjsachdeva.circleslifechatapp.util.ChatConstants;

public class MessageItem {



    public MessageItem(String ts, String direction, String msg) {

        ts = ts.substring(ChatConstants.START_INDEX,ChatConstants.END_INDEX);

        createdAt = ts;
        message = msg;
        if(ChatConstants.DIR_OUTGOING.equalsIgnoreCase(direction)) {
            isSent = true;
        }
    }


    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public boolean isSent() {
        return isSent;
    }

    public void setSent(boolean sent) {
        isSent = sent;
    }

    private String message;
    private String createdAt;
    private boolean isSent;

}
