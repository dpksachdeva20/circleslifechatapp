package com.example.dpkjsachdeva.circleslifechatapp.ui.fragment;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;

import com.example.dpkjsachdeva.circleslifechatapp.R;
import com.example.dpkjsachdeva.circleslifechatapp.adapter.MessageListAdapter;
import com.example.dpkjsachdeva.circleslifechatapp.model.MessageItem;
import com.example.dpkjsachdeva.circleslifechatapp.util.Utils;
import com.example.dpkjsachdeva.circleslifechatapp.viewmodel.MessageDataViewModel;

import java.util.List;

import static com.example.dpkjsachdeva.circleslifechatapp.util.ChatConstants.DIR_OUTGOING;
import static com.example.dpkjsachdeva.circleslifechatapp.util.ChatConstants.ONE_MIN;
import static com.example.dpkjsachdeva.circleslifechatapp.util.ChatConstants.UPDATE_LIST;
import static com.example.dpkjsachdeva.circleslifechatapp.util.ChatConstants.WAIT_MESSAGE;

public class ChatFragment extends Fragment {

    public static final String TAG = ChatFragment.class.getName();

    private RecyclerView mRecyclerView;
    private MessageDataViewModel msgDataViewModel;
    private MessageListAdapter messageListAdapter;
    private EditText etChatBox;
    private ImageView btnSend;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.chat_fragment, container, false);
        initView(view);

        return view;

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    public static ChatFragment newInstance() {
        return new ChatFragment();
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initViewModel();
        setData();

    }


    private void setData() {

        messageListAdapter = new MessageListAdapter(getActivity(), msgDataViewModel.getMsgLiveList().getValue());
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mRecyclerView.setAdapter(messageListAdapter);
    }

    private void initView(View view) {
        mRecyclerView = view.findViewById(R.id.reyclerview_message_list);
        etChatBox = view.findViewById(R.id.et_chatbox);
        btnSend = view.findViewById(R.id.sendButton);

        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!TextUtils.isEmpty(etChatBox.getText())) {
                    Message msg = new Message();
                    msg.what = UPDATE_LIST;
                    msg.obj = etChatBox.getText().toString();
                    mHandler.sendMessage(msg);
                    etChatBox.setText("");
                }

            }
        });

        mRecyclerView.addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
            @Override
            public void onLayoutChange(View v, int left, int top, int right, int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {
                if (bottom < oldBottom) {
                    mRecyclerView.post(new Runnable() {
                        @Override
                        public void run() {
                            // It is needed to scroll chat list to end when keyboard pops up..
                            scrollToEnd();
                        }
                    });
                }
            }
        });

    }

    /**
     * ButtonViewModel, and has observer to listen to changes in data
     */
    private void initViewModel() {

        msgDataViewModel = ViewModelProviders.of(this).get(MessageDataViewModel.class);
        msgDataViewModel.getMsgLiveList().observe(this, new Observer<List<MessageItem>>() {
            @Override
            public void onChanged(@Nullable List<MessageItem> msgList) {

                Log.d(TAG, "List updated with " + msgList.toString());

                updateView(msgList);
            }
        });
    }

    private void updateView(final List<MessageItem> msgList) {
        messageListAdapter.setMessageItemsList(msgList);
        if (mHandler.hasMessages(UPDATE_LIST)) {
            mHandler.removeMessages(UPDATE_LIST);
        }

        if (msgList != null && msgList.size() > 0) {
            MessageItem msgItem = msgList.get(msgList.size() - 1);
            if (!WAIT_MESSAGE.equalsIgnoreCase(msgItem.getMessage())) {
                Message msg = new Message();
                msg.what = UPDATE_LIST;
                msg.obj = WAIT_MESSAGE;
                mHandler.sendMessageDelayed(msg, ONE_MIN);
            }
        }
        scrollToEnd();

    }

    private final Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case UPDATE_LIST:
                    String msgText = (String) msg.obj;
                    String time = Utils.getDate(System.currentTimeMillis());

                    MessageItem messageItem = new MessageItem(time, DIR_OUTGOING, msgText);


                    msgDataViewModel.addMessage(messageItem);
            }
            super.handleMessage(msg);
        }
    };

    private void scrollToEnd() {
        mRecyclerView.scrollToPosition(messageListAdapter.getItemCount() - 1);

    }


}
