package com.example.dpkjsachdeva.circleslifechatapp.loader;

import android.arch.lifecycle.MutableLiveData;
import android.content.res.AssetManager;
import android.os.AsyncTask;

import com.example.dpkjsachdeva.circleslifechatapp.application.ChatApplication;
import com.example.dpkjsachdeva.circleslifechatapp.model.MessageItem;
import com.example.dpkjsachdeva.circleslifechatapp.util.ChatConstants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class FetchMessageDataAsyncTask extends AsyncTask<Void, Void, List<MessageItem>> {

    private static final String TAG = FetchMessageDataAsyncTask.class.getName();
    private MutableLiveData<List<MessageItem>> mutableLiveDataMsgList;

    public FetchMessageDataAsyncTask(MutableLiveData<List<MessageItem>> msgList) {
        mutableLiveDataMsgList = msgList;
    }

    @Override
    protected List<MessageItem> doInBackground(Void... voids) {
        // load message list from json or mock the json response in any dummy api..
        AssetManager manager = ChatApplication.getInstance().getAssets();
        InputStream file;
        try {
            file = manager.open(ChatConstants.JSON_FILE);
            byte[] formArray = new byte[file.available()];
            file.read(formArray);
            file.close();
            return getDialogsListFromString(new String(formArray));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(List<MessageItem> newsEntityList) {
        if(newsEntityList == null)
            return;

        super.onPostExecute(newsEntityList);
        if (mutableLiveDataMsgList.hasActiveObservers()) {
            mutableLiveDataMsgList.postValue(newsEntityList);
        }
    }

    /**
     * @return List of Button Models
     */
    private List<MessageItem> getDialogsListFromString(String str) {
        List<MessageItem> messageItemList = new ArrayList<>();
        try {
            JSONObject object = new JSONObject(str);
            JSONArray array = object.getJSONArray(ChatConstants.KEY_CHAT);
            for (int i = 0; i < array.length(); i++) {
                JSONObject itemObject = array.getJSONObject(i);
                MessageItem model = new MessageItem(itemObject.optString(ChatConstants.TIMESTAMP), itemObject.optString(ChatConstants.DIRECTION), itemObject.optString(ChatConstants.MESSAGE));
                messageItemList.add(model);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return messageItemList;
    }
}
