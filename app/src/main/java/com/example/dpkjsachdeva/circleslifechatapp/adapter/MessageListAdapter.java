package com.example.dpkjsachdeva.circleslifechatapp.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.dpkjsachdeva.circleslifechatapp.R;
import com.example.dpkjsachdeva.circleslifechatapp.model.MessageItem;
import com.example.dpkjsachdeva.circleslifechatapp.util.ChatConstants;

import java.util.ArrayList;
import java.util.List;

public class MessageListAdapter extends RecyclerView.Adapter<MessageListAdapter.ChatSentReceiveViewHolder> {

    private final LayoutInflater mInflater;
    private final Context mContext;
    private List<MessageItem> messageItemsList;

    public MessageListAdapter(Context context, List<MessageItem> list) {
        mInflater = LayoutInflater.from(context);
        mContext = context;
        messageItemsList = list;
        if(messageItemsList == null) {
            messageItemsList = new ArrayList<>();
        }


    }

    @NonNull
    @Override
    public ChatSentReceiveViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;
        if(viewType == ChatConstants.VIEW_TYPE_MSG_SENT) {
            view = inflateView(R.layout.chat_item_sent, parent, false);
        } else {
            view = inflateView(R.layout.chat_item_received, parent, false);

        }

        return new ChatSentReceiveViewHolder(view);
    }

    @Override
    public int getItemViewType(int position) {
        MessageItem messageItem = messageItemsList.get(position);
        if(messageItem.isSent()) {
            return ChatConstants.VIEW_TYPE_MSG_SENT;
        } else {
            return ChatConstants.VIEW_TYPE_MSG_RECEIVE;
        }
    }

    private View inflateView(int resource, ViewGroup root, boolean attachToRoot) {
       return mInflater.inflate(resource, root, attachToRoot);
   }

    @Override
    public void onBindViewHolder(@NonNull ChatSentReceiveViewHolder holder, int position) {
            MessageItem messageItem = messageItemsList.get(position);
              holder.bindMsgData(messageItem);
    }

    public void setMessageItemsList(List<MessageItem> msgList) {
        //udpate list item
        if(messageItemsList == null) {

            messageItemsList = new ArrayList<>();
        }

        messageItemsList = msgList;
        notifyDataSetChanged();
    }

    public void addMessage(MessageItem item) {
        messageItemsList.add(item);
        notifyItemInserted(messageItemsList.size());
    }

    @Override
    public int getItemCount() {
        if(messageItemsList == null)
            return 0;
        return messageItemsList.size();
    }

    static class ChatSentReceiveViewHolder extends RecyclerView.ViewHolder {
        private TextView tvBody;
        private TextView tvTime;
        private ImageView imgProfile;
        public ChatSentReceiveViewHolder(View itemView) {
            super(itemView);
            tvBody = itemView.findViewById(R.id.text_message_body);
            tvTime = itemView.findViewById(R.id.text_message_time);
            imgProfile = itemView.findViewById(R.id.image_message_profile);
        }

        private void bindMsgData(MessageItem messageItem) {
            tvTime.setText(messageItem.getCreatedAt());
            tvBody.setText(messageItem.getMessage());

            if(!messageItem.isSent()) {
                bindReceiveMsgData(messageItem);
            }
        }

        private void bindReceiveMsgData(MessageItem messageItem) {

        }
    }
}
