package com.example.dpkjsachdeva.circleslifechatapp.util;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Utils {

    public static String getDate(long time) {

        Date date = new Date();
        date.setTime(time);
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        String date1 = format.format(date);
        return date1;
    }

}
