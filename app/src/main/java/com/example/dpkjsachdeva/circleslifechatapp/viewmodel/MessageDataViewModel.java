package com.example.dpkjsachdeva.circleslifechatapp.viewmodel;

import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import com.example.dpkjsachdeva.circleslifechatapp.loader.FetchMessageDataAsyncTask;
import com.example.dpkjsachdeva.circleslifechatapp.model.MessageItem;

import java.util.ArrayList;
import java.util.List;

public class MessageDataViewModel extends ViewModel {

    private MutableLiveData<List<MessageItem>> liveMessageList;

    public MessageDataViewModel() {
        liveMessageList = new MutableLiveData<>();
        List<MessageItem> msgList = new ArrayList<>();
        liveMessageList.postValue(msgList);
    }

    public MutableLiveData<List<MessageItem>> getMsgLiveList() {
        if(liveMessageList.getValue() == null || liveMessageList.getValue().size() == 0) {
            //AsyncTask will be called only once for configuration changed.. and data will be saved till app got killed.
            //We can use DB as well to store the data.
            new FetchMessageDataAsyncTask(liveMessageList).execute();
        }
        return liveMessageList;
    }

    public void addMessage(MessageItem msg) {
        List<MessageItem> messageItemList = liveMessageList.getValue();
        messageItemList.add(msg);
        liveMessageList.setValue(messageItemList);
    }

}
