package com.example.dpkjsachdeva.circleslifechatapp.util.model;

import com.example.dpkjsachdeva.circleslifechatapp.model.MessageItem;
import com.example.dpkjsachdeva.circleslifechatapp.util.ChatConstants;
import com.example.dpkjsachdeva.circleslifechatapp.util.Utils;

import org.junit.Before;
import org.junit.Test;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertTrue;

public class MessageItemTest {

    private MessageItem messageItem;
    String time = Utils.getDate(System.currentTimeMillis());
    private final String TEXT_MSG = "Circles life is doing a good job";
    private final String MSG_OUT_DIR = ChatConstants.DIR_OUTGOING;
    private final String MSG_IN_DIR = ChatConstants.DIR_INCOMING;


    @Before
    public void initMessageItem() {
        messageItem = new MessageItem(time, MSG_OUT_DIR, TEXT_MSG);
    }

    @Test
    public void validateMessageText() {
        assertEquals(messageItem.getMessage(), TEXT_MSG);   // Date should be same after formatting

    }

    @Test
    public void validateMsgOutGoingDirection() {
        if(MSG_OUT_DIR.equalsIgnoreCase(ChatConstants.DIR_OUTGOING)) {
            assertTrue(messageItem.isSent());
        } else {
            assertTrue(!messageItem.isSent());

        }
    }

    @Test
    public void validateMsgIncomingDirection() {
        if(MSG_IN_DIR.equalsIgnoreCase(ChatConstants.DIR_INCOMING)) {
            assertTrue(messageItem.isSent());
        } else {
            assertTrue(!messageItem.isSent());

        }
    }

    @Test
    public void validateMsgTime() {

        // Date should be same after formatting
        assertEquals(messageItem.getCreatedAt(), time.substring(ChatConstants.START_INDEX, ChatConstants.END_INDEX));   // Date should be same after formatting

    }
}
