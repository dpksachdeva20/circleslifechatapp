package com.example.dpkjsachdeva.circleslifechatapp.util;

import org.junit.Test;

import java.text.SimpleDateFormat;
import java.util.Date;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotSame;

public class UtilsTest {

    @Test
    public void dateFormatValidator_IsCorrect() {
       long time = System.currentTimeMillis();
        String strDate = Utils.getDate(time);

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        Date expDate = new Date(time);
        String expDateStr = format.format(expDate);
        assertEquals(strDate, expDateStr);   // Date should be same after formatting

    }

    @Test
    public void dateFormatValidator_IsNotCorrect() {
        long time = System.currentTimeMillis();
        String strDate = Utils.getDate(time);

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");

        long time1 = System.currentTimeMillis();
        Date expDate = new Date(time1);
        String expDateStr = format.format(expDate);
        assertNotSame(strDate, expDateStr);   // Dates should not be same here...

    }
}
